var express = require('express');
var bodyParser = require("body-parser");
var userFile = require('./user.json');
var port = process.env.PORT || 3000;
var app = express();
var URLbase = '/apiperu/v1/';

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());


//GET users
// app.get(URLbase + 'users/', function(req, res) {
//   console.log('GET /apiperu/v1/users');
//   /*res.status(200).send({
//     "msg": "respuesta correcta"
//   });*/
//   res.status(200).send(userFile);
// });

// GET users by // ID
// app.get(URLbase + 'users/:id', function(req, res) {
//   console.log('GET /apiperu/v1/users/:id');
//   console.log(req.params);
//   console.log(req.params.id);
//   let pos = req.params.id;
//   let response = (userFile[pos - 1] == undefined) ? {"msg":"No existe"} : userFile[pos-1];
//   res.send(response);
// });

// GET con query string
app.get(URLbase + 'users/:id1/:id2',
  function(req, res) {
    console.log(req.query);
    console.log(req.params);
  });


//POST users
app.post(URLbase + 'users', function(req, res) {
  console.log('POST /apiperu/v1/users');
  let newID = userFile.length +1 ;
  let newUser = {
    "userID": newID,
    "first_name":req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password":req.body.password,
    "ip_address":req.body.ip_address
  }
  userFile.push(newUser);
  console.log(userFile);
  res.status(201).send({"msg": "usuario añadido"});

});

// DELETE USER
app.delete(URLbase+ 'users/:id', function (req, res){
  console.log('DELETE /apiperu/v1/users/:id');
  let pos = req.params.id-1;
    userFile.splice (pos,1);
    res.status(201).send({"msg": "usuario eliminado"});
});

// PUT USER
app.put(URLbase+ 'users/:id', function (req, res){
  console.log('PUT /apiperu/v1/users/:id');

  let modifiedID = req.params.id - 1 ;
  let modifiedUser = {
    "userID": modifiedID,
    "first_name":req.body.first_name,
    "last_name": req.body.last_name,
    "email": req.body.email,
    "password":req.body.password,
    "ip_address":req.body.ip_address
  }
  userFile.splice(modifiedID,1,modifiedUser)
  console.log(userFile);
  res.status(201).send({"msg": "usuario modificado"});
});



app.get('/', function(req, res) {
  res.send('Prueba Ruddy Cuellar Mayorga');
});

app.get('users/:id', function(req, res) {
  //res.send('user ' + req.params.id);
  res.json('user ' + req.params.id);
});

app.post(URLbase + 'login', function(req, res) {
  var user_name = req.body.user;
  var password = req.body.password;
  console.log("User name = " + user_name + ", password is " + password);
  res.json('Usuario correcto');
});



/*
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});*/


app.listen(port);
