var express = require('express');
var bodyParser = require("body-parser");
var requestJson = require("request-json");
var userFile = require('./user.json');
var port = process.env.PORT || 3000;
var app = express();
var URLbase = '/apiperu/v1/';
var urlMlabRaiz = "https://api.mlab.com/api/1/databases/pruebaapitechu/collections/";
var apiKey = "apiKey=mEyTh1j1tYyKl_s0lUGpMgX6b0THtNey";

app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(bodyParser.json());


//GET users
// app.get(URLbase + 'users/', function(req, res) {
//   console.log('GET /apiperu/v1/users');
//   /*res.status(200).send({
//     "msg": "respuesta correcta"
//   });*/
//   res.status(200).send(userFile);
// });

// GET users by // ID
// app.get(URLbase + 'users/:id', function(req, res) {
//   console.log('GET /apiperu/v1/users/:id');
//   console.log(req.params);
//   console.log(req.params.id);
//   let pos = req.params.id;
//   let response = (userFile[pos - 1] == undefined) ? {"msg":"No existe"} : userFile[pos-1];
//   res.send(response);
// });

// GET con query string
app.get(URLbase + 'users/:id1/:id2',
  function(req, res) {
    console.log(req.query);
    console.log(req.params);
  });

app.get('/apitechu/v5/usuarios', function(req,res) {
  let queryString = 'f={"_id":0}'
  let httpClient = requestJson.createClient(urlMlabRaiz);
  let response;
  httpClient.get('user?' + queryString + apiKey, function(err, resM, body) {
    if(err) {
      response = {
        "msg" : "Error obteniendo usuario."
      }
      res.status(500);
    } else {
      if(body.length > 0) {
        response = body;
      } else {
        response = {
          "msg" : "Ningun elemento user."
        }
        res.status(404);
      }
    }
    res.send(response);
  })
});

app.get('/apitechu/v5/usuarios/:id', function(req, res) {
  let query = 'q={"userID":' + req.params.id + '}&';
  let httpClient = requestJson.createClient(urlMlabRaiz);
  let response;
  httpClient.get('user?' + query + apiKey, function(err, resM, body) {
    if(err) {
      response = {
        "msg" : "Error obteniendo usuario."
      }
      res.status(500);
    } else {
      if(body.length > 0) {
        response = body;
      } else {
        response = {
          "msg" : "Ningun elemento user."
        }
        res.status(404);
      }
    }
    res.send(response);
  })
});

// PUT USER
app.put('/apitechu/v5/usuarios/:id', function (req, res){
  let id = req.params.id;
  let query = 'q={"userID":' + id + '}&';
  let httpClient = requestJson.createClient(urlMlabRaiz);
  httpClient.get("user?" + query + apiKey, function(err, respuestaMLab, body) {
      if (!err) {
        console.log(body);
        let data = '{ "$set" : ' + JSON.stringify(req.body) + ' }';
        httpClient.put("user?" + query + apiKey, JSON.parse(data), function(err, respuestaMLab, body) {
          res.send(body);
        });
      }
    });
});

//POST users
app.post('/apitechu/v5/usuarios', function(req, res) {
  let httpClient = requestJson.createClient(urlMlabRaiz);
  let queryStringContador = 'f={"_id":0}&c=true&'
  let newID = 0;
  httpClient.get('user?' + queryStringContador + apiKey, function(err, resM, body) {
    let loquesea = body;
    console.log("body = " +loquesea);
    newID = newID + loquesea + 1;
    let newUser = {
      "userID": newID,
      "first_name":req.body.first_name,
      "last_name": req.body.last_name,
      "email": req.body.email,
      "password":req.body.password,
      "ip_address":req.body.ip_address
    }
    console.log("newID = " +newID);
    httpClient.post("user?" + apiKey, JSON.parse(JSON.stringify(newUser)), function(err, respuestaMLab, body) {
        if (!err) {
          console.log(body);
          res.status(201).send({"msg": "usuario añadido"});
        }
      });
  });
});

// DELETE USER
app.delete('/apitechu/v5/usuarios/:id', function (req, res){
  let userID = req.params.id;
  var queryStringID = 'q={"id":' + id + '}&';
  let httpClient = requestJson.createClient(urlMlabRaiz);
  httpClient.get("user?" + queryStringID + apiKey, function(error, resMLab , body) {
    let respuesta = body[0];
    console.log("body delete:"+ respuesta);
    httpClient.delete("user/" + respuesta._id.$oid +'?'+ apiKey,
         function(error, resMLab,body){
           res.status(201).send(body);
         });
  });
});

app.put('/apitechu/v5/login', function (req, res) {
  let email = req.body.email;
  let pwd = req.body.password;
  let httpClient = requestJson.createClient(urlMlabRaiz);
  let queryLogin = 'q={"email":"' + email + '","password":"' + pwd + '"}&';
  let limFilter = 'l=1&';
  httpClient.get("user?" + queryLogin + limFilter + apiKey, function(err, resMLab, body) {
    if(!err) {
      if(body.length == 1) {
        let login = '{ "$set" : { "logged" : true } }';
        let respuesta = body[0];
        httpClient.put('user/' + respuesta._id.$oid + '?' + apiKey, JSON.parse(login), function(errP, resMLab, bodyP) {
          if(!errP) {
              res.send({'msg' : 'Login correcto', 'email' : body[0].email});
          } else {
              res.status(404).send({'msg' : 'Login incorrecto'});
          }
        });
      } else {
        res.status(404).send({'msg' : 'Usuario no encontrado'});
      }
    } else {
      res.status(404).send({'msg' : 'Error en la consulta del usuario'});
    }
  });
});

app.put('/apitechu/v5/logout', function (req, res) {
  let email = req.body.email;
  let httpClient = requestJson.createClient(urlMlabRaiz);
  let queryLogout = 'q={"email":"' + email + '","logged": true }&';
  let limFilter = 'l=1&';
  httpClient.get("user?" + queryLogout + limFilter + apiKey, function(err, resMLab, body) {
    if(!err) {
      if(body.length == 1) {
        let logout = '{ "$unset" : { "logged" : true } }';
        let respuesta = body[0];
        httpClient.put('user/' + respuesta._id.$oid + '?' + apiKey, JSON.parse(logout), function(errP, resMLab, bodyP) {
          if(!errP) {
              res.send({'msg' : 'Logout correcto', 'email' : body[0].email});
          } else {
              res.status(404).send({'msg' : 'Logout incorrecto'});
          }
        });
      } else {
        res.status(404).send({'msg' : 'Usuario no encontrado'});
      }
    } else {
      res.status(404).send({'msg' : 'Error en la consulta del usuario'});
    }
  });
});

app.get('/', function(req, res) {
  res.send('Prueba Ruddy Cuellar Mayorga');
});

app.post(URLbase + 'login', function(req, res) {
  var user_name = req.body.user;
  var password = req.body.password;
  console.log("User name = " + user_name + ", password is " + password);
  res.json('Usuario correcto');
});



/*
app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});*/


app.listen(port);
